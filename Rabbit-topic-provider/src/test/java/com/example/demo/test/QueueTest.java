package com.example.demo.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.GoodsSendInfo;
import com.example.demo.OrderSendInfo;
import com.example.demo.SpringBootRabbitMqApplication;
import com.example.demo.UserSendInfo;

/**
 * 消息队列测试类
 * @author heyuhome
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=SpringBootRabbitMqApplication.class)
public class QueueTest {
	
	@Autowired
	private UserSendInfo usendInfo;
	@Autowired
	private GoodsSendInfo gsendInfo;
	@Autowired
	private OrderSendInfo osendInfo;
	
	@Test
	public void test1() {
		usendInfo.sendInfo("用户aaa");
		gsendInfo.sendInfo("商品bbb");
		osendInfo.sendInfo("订单ccc");
		
		
	}
	
	
//	@Test
//	public void Test2() {
//		ExecutorService es = Executors.newFixedThreadPool(5);
//    	ReentrantLock lock = new ReentrantLock();
//    	
//
//        //加入5个任务
//        for(int i=1 ; i<1000; i++){
//            final int task = i;
//            es.execute(new Runnable() {
//                @Override
//                public void run() {
//                	lock.lock();
//                	try {
////                    for(int j=1; j<=2; j++){
////                       // System.out.println("现在运行的是第【 " + task + "】任务");
////                       //System.out.println(Thread.currentThread().getName() + "is work , now loop to " + j);
////                        
////                    	sendInfo.sendInfo("oskojscdn");
////                    	if(j==2){
////                            System.out.println("任务 【" + task + "】运行完成");
////                        }
////                        try {
////                            Thread.sleep(100);
////                        } catch (InterruptedException e) {
////                            e.printStackTrace();
////                        }
////                    }
//                		System.out.println("############"+Thread.currentThread().getName() + "is work");
//                		sendInfo.sendInfo("oskojscdn");
//                	}catch(Exception e) {
//                		e.printStackTrace();
//                		
//                	}finally {
//                		lock.unlock();
//					}
//                }
//            });
//        }
//       es.shutdown();
//       //countDownLatch.await();
//	}

}
