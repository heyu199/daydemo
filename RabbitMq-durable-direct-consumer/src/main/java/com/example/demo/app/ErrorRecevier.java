package com.example.demo.app;

import javax.management.RuntimeErrorException;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息接收
 * @author heyuhome
 *bindings 绑定队列
 *autoDelete="ture" 设置消费者失败情况下，是否默认清楚队列
 */
@Component
@RabbitListener(bindings=@QueueBinding(value=@Queue(value="${mq.config.queue.error}",autoDelete="ture")
   ,exchange=@Exchange(value="${mq.config.exchange}",type=ExchangeTypes.DIRECT)
   ,key="${mq.config.queue.error.routing.key}"))
public class ErrorRecevier {
	/**
	 * 监听数据
	 * @param msg
	 */
    @RabbitHandler
	public void recever( String msg) {
    	
		System.out.println("error*****" +msg);
		throw new RuntimeException();
		
	}

}
