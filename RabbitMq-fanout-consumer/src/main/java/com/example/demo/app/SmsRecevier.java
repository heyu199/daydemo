package com.example.demo.app;

import org.springframework.amqp.core.ExchangeTypes;
import org.springframework.amqp.rabbit.annotation.Exchange;
import org.springframework.amqp.rabbit.annotation.Queue;
import org.springframework.amqp.rabbit.annotation.QueueBinding;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

/**
 * 消息接收
 * @author heyuhome
 *bindings 绑定队列
 */
@Component
@RabbitListener(bindings=@QueueBinding(value=@Queue(value="${mq.config.queue.sms}",autoDelete="true")
   ,exchange=@Exchange(value="${mq.config.exchange}",type=ExchangeTypes.FANOUT)
   ))
public class SmsRecevier {
	/**
	 * 监听数据
	 * @param msg
	 */
    @RabbitHandler
	public void recever( String msg) {
		
		System.out.println("SMS*****" +msg);
		
	}

}
