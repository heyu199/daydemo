package com.java.demo.java.demo;

import java.util.Arrays;
import java.util.List;

public class LambdaTest {
	
	public static void main(String[] args) {
		
		LambdaTest.newT();

	}
			
			
			
	
	
	
	public static void oldT() {
		//Old way:
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
		for(Integer n: list) {
		   System.out.println(n);
	}
	}
	
	public static void newT() {
		List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7);
		list.forEach(n -> System.out.println(n));
	
    }
	}
