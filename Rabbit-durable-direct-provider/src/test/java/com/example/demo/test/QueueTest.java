package com.example.demo.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.SpringBootRabbitMqApplication;
import com.example.demo.app.SendInfo;

/**
 * 消息队列测试类
 * @author heyuhome
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=SpringBootRabbitMqApplication.class)
public class QueueTest {
	
	@Autowired
	private SendInfo sendInfo;
	
	@Test
	public void test3() throws InterruptedException {
		int flag=0;
//		while(true) {
//			flag++;
//			Thread.sleep(2000);
			sendInfo.sendInfo("持久化消息"+flag);

		//}
		
	}
	
	
	
}
