package com.example.demo.app;

import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
/**
 * 创建消息队列
 * @author heyuhome
 *
 */
@Configuration
public class QueueConfig {
	
	@Bean
	public Queue createQueue() {
		return new Queue("heyu00");
		
	}

}
