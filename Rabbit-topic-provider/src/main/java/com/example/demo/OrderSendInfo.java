package com.example.demo;

import javax.validation.Valid;

import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
/**
 * 消息发送者
 * @author heyuhome
 *
 */
@Component
public class OrderSendInfo {
	
	@Autowired
	private AmqpTemplate amqpTemplate;
	@Value("${mq.config.exchange}")
	private String exchange;
	/**
	 * 发送消息
	 * @param info
	 */
	public void sendInfo(String info) {
		/**
		 * heyu00 往哪发
		 * info 发什么
		 */
		System.out.println(exchange+"****");
		this.amqpTemplate.convertAndSend(exchange,"order.log.info", "orderinfo**"+info);
		this.amqpTemplate.convertAndSend(exchange,"order.log.debug", "orderdebug"+info);
		this.amqpTemplate.convertAndSend(exchange,"order.log.error", "ordererror"+info);
		this.amqpTemplate.convertAndSend(exchange,"order.log.warn", "orderwarn"+info);

	}

}
