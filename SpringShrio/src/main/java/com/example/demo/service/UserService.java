package com.example.demo.service;

import java.util.List;

import com.example.demo.entity.User;

public interface UserService {
    User save(User user);

    User findById(String id);

    void delete(User user);

    List<User> findAll();
}
