package com.example.demo.test;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.locks.ReentrantLock;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.demo.OrderSendInfo;
import com.example.demo.SpringBootRabbitMqApplication;

/**
 * 消息队列测试类
 * @author heyuhome
 *
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes=SpringBootRabbitMqApplication.class)
public class QueueTest {
	
	
	@Autowired
	private OrderSendInfo osendInfo;
	
	@Test
	public void test1() {
		osendInfo.sendInfo("测试fanout模式");
		
	}
	
}
