package com.java.demo.java.demo;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

/**
 * Hello world!
 *日常小demo
 */
public class App 
{
    public static void main( String[] args ) throws Exception
    {
      System.out.println("*********文件复制*********");
      App.copyFile("/Users/heyuHome/Desktop/index(3).html", "/Users/heyuHome/Desktop/index.html");
      
    }
    
    /**
     * 实现文件复制
     * @throws Exception 
     */
    public static void copyFile(String localPath,String destPathS) throws Exception {
    	
			FileInputStream inputStream =new FileInputStream(localPath);
			FileOutputStream outputStream=new FileOutputStream(destPathS);
			byte[] buffer = new byte[20 * 1024];
		    int cnt;

		    // read() 最多读取 buffer.length 个字节
		    // 返回的是实际读取的个数
		    // 返回 -1 的时候表示读到 eof，即文件尾
		    while ((cnt = inputStream.read(buffer, 0, buffer.length)) != -1) {
		    	outputStream.write(buffer, 0, cnt);
		    }

		    inputStream.close();
		    outputStream.close();
		
    	
    	
    }
}
