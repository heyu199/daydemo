package com.example.demo.ftp;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import org.apache.commons.net.ftp.FTPClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

@Component
public class FtpUtils {
//	@Value("${ftp.host}")
//	private String FTPHOST;
//	@Value("${ftp.port}")
//	private String FTPPORT;
//	@Value("${ftp.username}")
//	private String FTPUSERNAME;
//	@Value("${ftp.userpassword}")
//	private String FTPHOST;
	
	
	@Scheduled(fixedRate = 5000)
	public  void upwenjian() {
	    // 创建客户端对象
	    FTPClient ftpClient = new FTPClient();
	    InputStream local = null;
	    try {
	            // 连接ftp服务器
	            ftpClient.connect("49.234.184.31", 21);
	            // 用户名登入
	            ftpClient.login("root", "Heyu1994713");
	            // 设置上传路径
	            String path = "/home/image";
	            // 检查上传路径是否存在，不存在返回false
	            // boolean flag=ftpClient.changeWorkingDirectory(path);
	            // 不存在子目录进行创建
	            //获取当前
	            Date currentDate = new Date();
	            String dateStr = new SimpleDateFormat("yyyy/MM/dd").format(currentDate);
	            for (String pathStr : dateStr.split("/")) {
	              path += pathStr + "/";
	              boolean flag = ftpClient.changeWorkingDirectory(path);
	              if (!flag) {
	                ftpClient.makeDirectory(path);
	              }
	            }
	            // 指定上传路径
	            ftpClient.changeWorkingDirectory(path);
	            // 指定上传文件类型
	            ftpClient.setFileType(2);
	            // 读取本地文件
	            File file = new File("src/main/resources/yumi_paigutang.jpg");
	            local = new FileInputStream(file);
	            // 第一个参数是文件名
	            String filename = FtpUtils.getFileName(file.getName());
	            ftpClient.storeFile(filename, local);
	            System.out.println("*****任务执行***");
	    } catch (SocketException e) {
	      e.printStackTrace();
	    } catch (IOException e) {
	      e.printStackTrace();
	    } finally {
	      try {
	        // 关闭文件流
	        local.close();
	        // 退出
	        ftpClient.logout();
	        // 断开连接
	        ftpClient.disconnect();
	      } catch (IOException e) {
	        e.printStackTrace();
	      }
	    }

	  }

	  public static String getFileName(String primitiveFileName) {
	    // 使用uuid生成文件名
	    String fileName = UUID.randomUUID().toString();
	    // 获取文件后缀
	    String suffix = primitiveFileName.substring(primitiveFileName.lastIndexOf("."));
	    return fileName + suffix;
	  }


}
