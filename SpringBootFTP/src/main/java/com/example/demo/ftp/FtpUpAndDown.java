package com.example.demo.ftp;  
/**
 *  1----public boolean uploadFile(String path ,File localFile,String fileNewName);   上传文件  path  文件存入FTP 的路径, 
 *                        localFile  文件在本地的路径,fileNewName 文件上传FTP后名字  
 *                        
 *  2----public boolean downloadFileq(String path,String ftpName,File localFile);文件下载   @param ftpName  ftp上的文件名 ， localFile  保存的本地地址 
 *                        path ftp上的文件路径
 *                        
 *  3----public boolean deleteFile(String path,String ftpName)； 文件删除   path  ftp上的文件路径  
 *     
 *  4----public boolean findFile(String pathName,String  judge)； 获取指定文件夹下的文件目录    pathName  需要获取文件夹下文家目录的文件夹路径 , 
 *  				judge  是否进行下级文件夹遍历,"Y"选择遍历   "N"选择不遍历           
 * @author Dell
 *
 */ 
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.List;
 
 
import org.apache.commons.net.ftp.FTPClient;
import org.apache.commons.net.ftp.FTPFile;
 
import com.alibaba.fastjson.JSONObject;
 
 
public class FtpUpAndDown {  
    
    public static String ftpHost;  
    public static int port;  
    public static String userName;  
    public static String passWord;  
    public static String ftpEncode; 
    public static int defaultTimeout;  
    /** 
     * 静态块，初始化ftp数据 
     */  
    static{  
        try{  
            ftpHost = "49.234.184.31";
            port = 21;            
            userName = "root";  
            passWord = "Heyu1994713";  
            ftpEncode="UTF-8";  
            defaultTimeout = 30000;  
        }catch(Exception e){  
            e.printStackTrace();  
        }  
    }  
 
	/** 
     * 上传ftp 
     * @param path  文件存入FTP 的路径
     * @param localFile  文件在本地的路径
     * @param fileNewName 文件上传FTP后名字
     * @return 成功返回true   失败返回false
     * @throws SocketException 
     * @throws IOException 
     */  
    public String uploadFile(String path ,File localFile,String fileNewName) {  
        boolean flag=true;  
        //获得文件流  
        FileInputStream is;
		try {
			is = new FileInputStream(localFile);
			  //保存至Ftp  
	        FTPClient ftpClient = new FTPClient();// ftpHost为FTP服务器的IP地址，port为FTP服务器的登陆端口,ftpHost为String型,port为int型。  
	        ftpClient.setControlEncoding(ftpEncode); // 中文支持  
	        ftpClient.connect(ftpHost);  
	        ftpClient.login(userName, passWord);// userName、passWord分别为FTP服务器的登陆用户名和密码  
	        ftpClient.setDefaultPort(port);  
	        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);  
	        ftpClient.enterLocalPassiveMode(); // 用被动模式传输,解决linux服务长时间等待，导致超时问题  
	        ftpClient.setDefaultTimeout(defaultTimeout);// 设置默认超时时间  
	        ftpClient.setBufferSize(1024*1024);//设置缓存区，默认缓冲区大小是1024，也就是1K  
	        //切换目录，目录不存在创建目录  
	        boolean chagenDirFlag=ftpClient.changeWorkingDirectory(path);  
	        if(chagenDirFlag==false){  
	            ftpClient.makeDirectory(path);  
	            ftpClient.changeWorkingDirectory(path);  
	        }  
	        //上传至Ftp  
	        flag=ftpClient.storeFile(fileNewName, is);  
	        is.close();  
	        //关闭连接  
	        ftpClient.logout();  
	        ftpClient.disconnect();  
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			return "FileNotFound";
		} catch (SocketException e) {
			// TODO Auto-generated catch block
			return "SocketNotSuccess";
		} catch (IOException e) {
			// TODO Auto-generated catch block
			return "SocketNotSuccess";
		}  
        return "SUCCESS";  
    }  
      
    /** 
     * 下载FTP 
     * @param ftpName  ftp上的文件名 
     * @param localFile  保存的本地地址 
     * @param path ftp上的文件路径
     *  @return 成功返回true   失败返回false
     * @throws SocketException 
     * @throws IOException 
     */  
    public String  downloadFileq(String path,String ftpName,File localFile) {  
        boolean flag=true;  
        //保存至Ftp  
        FTPClient ftpClient = new FTPClient();// ftpHost为FTP服务器的IP地址，port为FTP服务器的登陆端口,ftpHost为String型,port为int型。  
        ftpClient.setControlEncoding(ftpEncode); // 中文支持  
        try {
			ftpClient.connect(ftpHost);
			 ftpClient.login(userName, passWord);// userName、passWord分别为FTP服务器的登陆用户名和密码  
		        ftpClient.setDefaultPort(port);  
		        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);  
		        ftpClient.enterLocalPassiveMode(); // 用被动模式传输,解决linux服务长时间等待，导致超时问题  
		        ftpClient.setDefaultTimeout(defaultTimeout);// 设置默认超时时间  
		        ftpClient.setBufferSize(1024*1024);//设置缓存区，默认缓冲区大小是1024，也就是1K  
		        //切换目录，目录不存在创建目录  
		        ftpClient.changeWorkingDirectory(path);  
		        OutputStream os = new FileOutputStream(localFile);  
		        flag = ftpClient.retrieveFile(ftpName, os);  
		        //关闭流
		        os.flush();  
		        os.close();  
		        //关闭连接  
		        ftpClient.logout();  
		        ftpClient.disconnect();  
		        System.out.println("FTP文件名——"+ftpName);  
		} catch (SocketException e) {
			return "SocketNotSuccess";
		} catch (IOException e) {
			return "SocketNotSuccess";
		}  
        return "SUCCESS"; 
    }  
      
    /** 
     * 删除FTP 
     * @param ftpName  ftp上的文件名 
     *  @param path  ftp上的文件路径 
     *  @return 成功返回true   失败返回false
     * @throws SocketException 
     * @throws IOException 
     */  
    public String deleteFile(String path,String ftpName) {  
        boolean flag=true;  
        //保存至Ftp  
        FTPClient ftpClient = new FTPClient();// ftpHost为FTP服务器的IP地址，port为FTP服务器的登陆端口,ftpHost为String型,port为int型。  
        ftpClient.setControlEncoding(ftpEncode); // 中文支持  
        try {
			ftpClient.connect(ftpHost);
			 ftpClient.login(userName, passWord);// userName、passWord分别为FTP服务器的登陆用户名和密码  
		        ftpClient.setDefaultPort(port);  
		        ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);  
		        ftpClient.enterLocalPassiveMode(); // 用被动模式传输,解决linux服务长时间等待，导致超时问题  
		        ftpClient.setDefaultTimeout(defaultTimeout);// 设置默认超时时间  
		        //切换目录，目录不存在创建目录  
		        ftpClient.changeWorkingDirectory(path);  
		        flag = ftpClient.deleteFile(ftpName);  
		        //关闭连接  
		        ftpClient.logout();  
		        ftpClient.disconnect();  
		} catch (SocketException e) {
			return "SocketNotSuccess";
		} catch (IOException e) {
			return "SocketNotSuccess";
		}  
        return "SUCCESS"; 
    }  
    
    /** 
     * 查询指定目录下的文件名 
     *  @pathName path  ftp上的文件路径 
     *  @pathName judge  是否进行下级文件夹遍历    "Y"选择遍历   "N"选择不遍历       
     * @return  JSONObject    文件目录
     * @throws SocketException 
     * @throws IOException 
     */  
    public  JSONObject findFile(String pathName,String  judge) {  
    	  JSONObject json = new JSONObject();
        //保存至Ftp  
        FTPClient ftpClient = new FTPClient();// ftpHost为FTP服务器的IP地址，port为FTP服务器的登陆端口,ftpHost为String型,port为int型。  
        ftpClient.setControlEncoding(ftpEncode); // 中文支持  
        try {
			ftpClient.connect(ftpHost);
			ftpClient.login(userName, passWord);// userName、passWord分别为FTP服务器的登陆用户名和密码  
		    ftpClient.setDefaultPort(port);  
		    ftpClient.setFileType(FTPClient.BINARY_FILE_TYPE);  
		    ftpClient.enterLocalPassiveMode(); // 用被动模式传输,解决linux服务长时间等待，导致超时问题  
		    ftpClient.setDefaultTimeout(defaultTimeout);// 设置默认超时时间  
		    //遍历方法
		    List<String> res  =  new ArrayList<String>();
		    res= ListALL( pathName,ftpClient,judge);
			json.put("json", res);
		    //关闭连接  
		    ftpClient.logout();  
		    ftpClient.disconnect();  
		} catch (SocketException e) {
			json.put("Result", "SocketNotSuccess");
		} catch (IOException e) {
			json.put("Result", "SocketNotSuccess");
		}  
        return json;  
    }  
    
 
	/** 
     * 递归遍历出目录下面所有文件 
     * @param pathName 需要遍历的目录，必须以"/"开始和结束 
     * @return List<String>
     * @throws IOException 
     */  
	List<String> arFiles  =  new ArrayList<String>();
    public  List<String> ListALL(String pathName,FTPClient ftpClient,String judge){  
        if(pathName.startsWith("/")&&pathName.endsWith("/")){  
            String directory = pathName;  
            //更换目录到当前目录  
            try {
				ftpClient.changeWorkingDirectory(directory);
				 FTPFile[] files = ftpClient.listFiles();  
				   //遍历文件夹下所有文件
		            for(FTPFile file:files){  
		                if(file.isFile()){  //如果是文件
		                    arFiles.add(directory+file.getName());  
		                    //如果是文件夹并且拥有查询标示
		                }else if(file.isDirectory()&&judge.equals("Y")){  
		                    List(directory+file.getName()+"/",ftpClient,judge);  
		                }else if(file.isDirectory()&&judge.equals("N")){  
		                    continue; 
		                }    
		            }  
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}  
    }
		return arFiles;  
}
 
	/** 
     * 递归遍历出目录下面所有文件放入指定目录 
     * @param pathName 需要遍历的目录，必须以"/"开始和结束 
     * @return List<String>
     * @throws IOException 
     */  
    public void List(String pathName,FTPClient ftpClient,String judge) throws IOException{
		InputStream in = null;
        if(pathName.startsWith("/")&&pathName.endsWith("/")){  
            String directory = pathName;  
            //更换目录到当前目录  
            try {
				ftpClient.changeWorkingDirectory(directory);
				 FTPFile[] files = ftpClient.listFiles();  
				   //遍历文件夹下所有文件
		            for(FTPFile file:files){  
		                if(file.isFile()){  //如果是文件
							String remoteAbsoluteFile = file.getName();
		                    remoteAbsoluteFile = new String(remoteAbsoluteFile.getBytes("UTF-8"), "ISO-8859-1");
		                    in = ftpClient.retrieveFileStream(remoteAbsoluteFile);
							byteToFile(in, "D:/tomcat/apache-tomcat-7.0.85/webapps/service-app/fenci/"+remoteAbsoluteFile);
							System.out.println("下载成功!" + remoteAbsoluteFile);
		                  
		                    in.close();
		                    ftpClient.completePendingCommand();  
		                   //如果是文件夹并且拥有查询标示
		                }else if(file.isDirectory()&&judge.equals("Y")){  
		                    List(directory+file.getName()+"/",ftpClient,judge);  
		                }else if(file.isDirectory()&&judge.equals("N")){  
		                    continue; 
		                }    
		            }  
			} catch (IOException e) {				
				e.printStackTrace();
			}finally{
				in.close();
			}  
		}
		 
	}
	
	/**
    * byte[] 转 file 
    * 
    * @param bytes
    * @param path
    */
   public static void byteToFile(InputStream inStream, String path)
   {
       try
       {
           // 输出流InputStream inStream
			File localFile = new File(path);
			if(!localFile.exists()){
				if(!localFile.getParentFile().exists()) {
					localFile.getParentFile().mkdir();
				}
				localFile.createNewFile();
			}
			OutputStream outStream = new FileOutputStream(localFile);
			byte[] b = new byte[1024];
			int len = -1;
			while((len=inStream.read(b)) != -1){
				outStream.write(b, 0, len);
			}
			outStream.flush();
           inStream.close();
       }
       catch (Exception e)
       {
           e.printStackTrace();
       }
   }
    
    
    public static void main(String[] args) throws IOException { 
         
        FtpUpAndDown util = new FtpUpAndDown();  
        /*
        //上传测试  
        File localFile=new File("/Users/heyuHome/Documents/heyuFile/ugp_ne.csv");  
        String path = "/root/update/";
        String flag=util.uploadFile(path,localFile, "heyu.csv");  
        System.out.println(flag +"   判断上传结果");  */
          
        //下载测试  
        File localFile1=new File("/Users/heyuHome/Desktop/15.csv");  
        localFile1.createNewFile();  
        String flag1=util.downloadFileq("/root/update/","heyu.csv",localFile1);              
        System.out.println(flag1 +"   判断下载结果");  
//          
//        //删除测试  
//        String flag2=util.deleteFile("/root/update/","1.jpg");  
//        System.out.println(flag +"   判断删除结果");  
//        
//        //获取测试  
//    	JSONObject   json=util.findFile("/root/update/","Y");  
//        System.out.println(json.toJSONString() +"   文件夹下目录");  
        
    } 
}  